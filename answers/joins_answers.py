# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Join Expressions
# MAGIC A join brings together two sets of data, the left and the right, by comparing the value of one or more keys of the left and right and evaluating the result of a join expression that determines whether Spark should bring together the left set of data with the right set of data. The most common join expression, an equi-join, compares whether the specified keys in your left and right datasets are equal. If they are equal, Spark will combine the left and right datasets. The opposite is true for keys that do not match; Spark discards the rows that do not have matching keys. Spark also allows for much more sophsticated join policies in addition to equi-joins. We can even use complex types and perform something like checking whether a key exists within an array when you perform a join.

# COMMAND ----------

from pyspark.sql import functions as F

arbitrary_join_expression = F.col("a_id") == F.col("b_id")

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC - Inner joins (keep rows with keys that exist in the left and right datasets)
# MAGIC 
# MAGIC - Left outer joins (keep rows with keys in the left dataset). Also called a left join
# MAGIC 
# MAGIC - Right outer joins (keep rows with keys in the right dataset). Also called a right join
# MAGIC 
# MAGIC - Outer joins (keep rows with keys in either the left or right datasets)
# MAGIC 
# MAGIC - Cross (or Cartesian) joins (match every row in the left dataset with every row in the right dataset)
# MAGIC 
# MAGIC 
# MAGIC ![joins](/files/tables/join_types.png)

# COMMAND ----------

# MAGIC %md
# MAGIC Let's move on to showing examples of each join type. This will make it easy to understand exactly how you can apply these to your own problems. To do this, let’s create some simple datasets that we can use in our examples.

# COMMAND ----------

person = spark.createDataFrame([
    (0, "Bill Chambers", 0, [100]),
    (1, "Matei Zaharia", 1, [500, 250, 100]),
    (2, "Michael Armbrust", 1, [250, 100])])\
  .toDF("id", "name", "graduate_program", "spark_status")

graduateProgram = spark.createDataFrame([
    (0, "Masters", "School of Information", "UC Berkeley"),
    (2, "Masters", "EECS", "UC Berkeley"),
    (1, "Ph.D.", "EECS", "UC Berkeley")])\
  .toDF("id", "degree", "department", "school")

sparkStatus = spark.createDataFrame([
    (500, "Vice President"),
    (250, "PMC Member"),
    (100, "Contributor")])\
  .toDF("id", "status")

# COMMAND ----------

# MAGIC %md
# MAGIC # Inner Joins
# MAGIC Inner joins evaluate the keys in both of the DataFrames or tables and include (and join together) only the rows that evaluate to true. In the following example, we join the graduateProgram DataFrame with the person DataFrame to create a new DataFrame
# MAGIC 
# MAGIC ![joins](/files/tables/inner.png)

# COMMAND ----------

joinExpression = person["graduate_program"] == graduateProgram['id']

# COMMAND ----------

# MAGIC %md
# MAGIC Keys that do not exist in both DataFrames will not show in the resulting DataFrame. For example, the following expression would result in zero values in the resulting DataFrame.

# COMMAND ----------

wrongJoinExpression = person["name"] == graduateProgram["school"]

# COMMAND ----------

repr(joinExpression)

# COMMAND ----------

# MAGIC %md
# MAGIC Inner joins are the default join, so we just need to specify our left DataFrame and join the right in the JOIN expression.

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from inner join on: person["graduate_program"] == graduateProgram['id']""")
(
  person
  .join(
    graduateProgram,
    joinExpression
  )
  .show()
)

# COMMAND ----------

# MAGIC %md
# MAGIC Can also be done explicitely, from the [docs](https://spark.apache.org/docs/2.3.0/api/python/pyspark.sql.html#pyspark.sql.DataFrame.join) we see that a join in general is specified as:
# MAGIC 
# MAGIC `join(other, on=None, how=None)`:Joins with another DataFrame, using the given join expression.
# MAGIC 
# MAGIC Parameters:	
# MAGIC * `other` Right side of the join
# MAGIC * `on` A string for the join column name, a list of column names, a join expression (Column), or a list of Columns. If on is a string or a list of strings indicating the name of the join column(s), the column(s) must exist on both sides, and this performs an equi-join.
# MAGIC * `how` str, default inner. Must be one of: `inner`, `cross`, `outer`, `full`, `full_outer`, `left`, `left_outer`, `right`, `right_outer`, `left_semi`, and `left_anti`.

# COMMAND ----------

(
  person
    .join(
      other = graduateProgram,
      on = joinExpression,
      how = 'inner',
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Outer Joins
# MAGIC 
# MAGIC Outer joins evaluate the keys in both of the DataFrames or tables and includes (and joins together) the rows that evaluate to true or false. If there is no equivalent row in either the left or right DataFrame, Spark will insert null.
# MAGIC 
# MAGIC how = `outer`, `full`, `full_outer`, all specify a full outer join.
# MAGIC 
# MAGIC ![joins](/files/tables/full_outer.png)

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from outer join on: person["graduate_program"] == graduateProgram['id']""")
(
  person
  .join(
    graduateProgram,
    joinExpression,
    'outer'
  )
  .show()
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Left Outer Joins
# MAGIC Left outer joins evaluate the keys in both of the DataFrames or tables and includes all rows from the left DataFrame as well as any rows in the right DataFrame that have a match in the left DataFrame. If there is no equivalent row in the right DataFrame, Spark will insert null. Right outer join is analogous to a left outer join when reversing the dataframes.
# MAGIC 
# MAGIC how = `left`, `left_outer`, `right`, `right_outer` specify left and right joins.
# MAGIC 
# MAGIC ![joins](/files/tables/left_right.png)

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from left join on: person["graduate_program"] == graduateProgram['id']""")
(
  person
  .join(
    graduateProgram,
    joinExpression,
    'left',
  )
  .show()
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Cross (Cartesian) Joins
# MAGIC The last of our joins are cross-joins or cartesian products. Cross-joins in simplest terms are inner joins that do not specify a predicate. Cross joins will join every single row in the left DataFrame to ever single row in the right DataFrame. This will cause an absolute explosion in the number of rows contained in the resulting DataFrame. If you have 1,000 rows in each DataFrame, the cross-join of these will result in 1,000,000 (1,000 x 1,000) rows. For this reason, you must very explicitly state that you want a cross-join by using the cross join keyword.
# MAGIC 
# MAGIC ![joins](/files/tables/cross.png)

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from inner join on: person["graduate_program"] == graduateProgram['id']""")
(
  person
  .crossJoin(
    graduateProgram
  )
  .show()
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Left Semi and Left Anti Joins
# MAGIC 
# MAGIC Two extra useful join `left_semi` and `left_anti`.
# MAGIC 
# MAGIC - Left semi joins (keep the rows in the left, and only the left, dataset where the key appears in the right dataset)
# MAGIC 
# MAGIC - Left anti joins (keep the rows in the left, and only the left, dataset where they do not appear in the right dataset)
# MAGIC 
# MAGIC Semi joins are a bit of a departure from the other joins. They do not actually include any values from the right DataFrame. They only compare values to see if the value exists in the second DataFrame. If the value does exist, those rows will be kept in the result, even if there are duplicate keys in the left DataFrame. Think of left semi joins as filters on a DataFrame.
# MAGIC 
# MAGIC Left anti joins are the opposite of left semi joins. Like left semi joins, they do not actually include any values from the right DataFrame. They only compare values to see if the value exists in the second DataFrame. However, rather than keeping the values that exist in the second DataFrame, they keep only the values that do not have a corresponding key in the second DataFrame.
# MAGIC 
# MAGIC how = `left_semi`, `left_anti` specify a left semi and left anti join.

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from left semi join on: person["graduate_program"] == graduateProgram['id']""")
(
  person
  .join(
    graduateProgram,
    joinExpression,
    'left_semi'
  )
  .show()
)

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from left anti join on: person["graduate_program"] == graduateProgram['id']""")
(
  person
  .join(
    graduateProgram,
    joinExpression,
    'left_anti'
  )
  .show()
)

# COMMAND ----------

# MAGIC %md
# MAGIC They're logically equivalent to left joins followed by a filter.

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from analogous left semi join on: person["graduate_program"] == graduateProgram['id']""")
(
  person.alias('p')
  .join(
    graduateProgram,
    joinExpression,
    'left'
  )
  .where(
    graduateProgram['id'].isNotNull()
  )
  .select(
    'p.*'
  )
  .show()
)

# COMMAND ----------

print("person DataFrame")
person.show()

print("graduateProgram DataFrame")
graduateProgram.show()

print("""DataFrame from analogous left anti join on: person["graduate_program"] == graduateProgram['id']""")
(
  person.alias('p')
  .join(
    graduateProgram,
    joinExpression,
    'left'
  )
  .where(
    graduateProgram['id'].isNull()
  )
  .select(
    'p.*'
  )
  .show()
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Exercises
# MAGIC 
# MAGIC We will be using data that contains information about all flights that departed from NYC (e.g. EWR, JFK and LGA) in 2013: 336,776 flights in total.
# MAGIC 
# MAGIC - `flights` all flights that departed from NYC in 2013
# MAGIC - `weather` hourly meterological data for each airport
# MAGIC - `planes` construction information about each plane
# MAGIC - `airports` airport names and locations
# MAGIC - `airlines` translation between two letter carrier codes and names
# MAGIC 
# MAGIC ![joins](/files/tables/relational_nycflights-90119.png)

# COMMAND ----------

# MAGIC %md a. Download the data by running the below block, the magic function `%sh`, sends an input to the shell in the driver node.

# COMMAND ----------

# MAGIC %sh
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/airlines.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/airports.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/flights.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/planes.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/weather.csv'

# COMMAND ----------

localpaths= [
  "file:/tmp/airlines.csv",
  "file:/tmp/airports.csv",
  "file:/tmp/flights.csv",
  "file:/tmp/planes.csv",
  "file:/tmp/weather.csv",
]
dbutils.fs.mkdirs("dbfs:/datasets/")
list(map(lambda localpath: dbutils.fs.cp(localpath, "dbfs:/datasets/"), localpaths))
display(dbutils.fs.ls("dbfs:/datasets/"))

# COMMAND ----------

# MAGIC %md b. Read the csv files into spark DataFrame's.

# COMMAND ----------

airlines = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/datasets/airlines.csv")
)

airports = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/datasets/airports.csv")
)
display(airports)

# COMMAND ----------

display(airlines)

# COMMAND ----------

# MAGIC %md
# MAGIC a. Explain to youself *why* a left semi join is logically equivalent to a left join followed by a filter.

# COMMAND ----------

# MAGIC %md
# MAGIC b. 

# COMMAND ----------

dbutils.fs.ls('databricks-datasets/flights/airport-codes-na.txt')
