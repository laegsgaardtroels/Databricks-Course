# Databricks notebook source
dbutils.fs.ls('dbfs:/')


# COMMAND ----------

from pyspark.sql import functions as F

df = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/databricks-datasets/definitive-guide/data/retail-data/all/*.csv")
  .coalesce(5)
)
df.cache()
df.createOrReplaceTempView("dfTable")

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Aggregation Functions
# MAGIC 
# MAGIC Aggregation functions are available in the `pyspark.sql.functions` module. It is good practice to give this module an alias and refer to it that way PEP.

# COMMAND ----------

# MAGIC %md
# MAGIC ## Download the data

# COMMAND ----------

from pyspark.sql import functions as F

df = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/databricks-datasets/definitive-guide/data/retail-data/all/*.csv")
  .coalesce(5)
)
df.cache()
df.createOrReplaceTempView("dfTable")

# COMMAND ----------

# MAGIC %md 
# MAGIC ## count
# MAGIC 
# MAGIC The first function worth going over is count, except in this example it will perform as a transformation instead of an action. In this case, we can do one of two things: specify a specific column to count, or all the columns by using count(*) or count(1) to represent that we want to count every row as the literal one, when performing a count(*), Spark will count null values (including rows containing all nulls). However, when counting an individual column, Spark will not count the null values.

# COMMAND ----------

df_ = df.select(F.count('*').alias("row_count"))

# COMMAND ----------

# MAGIC %md
# MAGIC Now call an action:

# COMMAND ----------

display(df_)

# COMMAND ----------

# MAGIC %md
# MAGIC ## countDistinct
# MAGIC Sometimes, the total number is not relevant; rather, it’s the number of unique groups that you want. To get this number, you can use the countDistinct function. This is a bit more relevant for individual column.

# COMMAND ----------

display(
  df.select(F.countDistinct("StockCode"))
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ##First and Last
# MAGIC You can get the first and last values from a DataFrame by using these two obviously named functions. This will be based on the rows in the DataFrame, not on the values in the DataFrame

# COMMAND ----------

display(
  df.select(F.first("StockCode"), F.last("StockCode"))
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## min and max
# MAGIC To extract the minimum and maximum values from a DataFrame, use the min and max functions

# COMMAND ----------

display(
  df.select(F.min("Quantity"), F.max("Quantity"))
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## sum
# MAGIC Another simple task is to add all the values in a row using the sum function

# COMMAND ----------

display(
  df.select(F.sum("Quantity"))
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## avg
# MAGIC Although you can calculate average by dividing sum by count, Spark provides an easier way to get that value via the avg or mean functions. 

# COMMAND ----------

display(
  df.select(F.mean("Quantity"))
)

# COMMAND ----------

# MAGIC %md 
# MAGIC ## Aggregating to Complex Types
# MAGIC In Spark, you can perform aggregations not just of numerical values using formulas, you can also perform them on complex types. For example, we can collect a list of values present in a given column or only the unique values by collecting to a set.
# MAGIC 
# MAGIC You can use this to carry out some more programmatic access later on in the pipeline or pass the entire collection in a user-defined function (UDF).

# COMMAND ----------

df.agg(F.collect_set("Country"), F.collect_list("Country")).show()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #Grouping
# MAGIC Thus far, we have performed only DataFrame-level aggregations. A more common task is to perform calculations based on groups in the data. This is typically done on categorical data for which we group our data on one column and perform some calculations on the other columns that end up in that group.
# MAGIC 
# MAGIC The best way to explain this is to begin performing some groupings. The first will be a count, just as we did before. We will group by each unique invoice number and get the count of items on that invoice. Note that this returns another DataFrame and is lazily performed.
# MAGIC 
# MAGIC We do this grouping in two phases. First we specify the column(s) on which we would like to group, and then we specify the aggregation(s).

# COMMAND ----------

df.groupBy("InvoiceNo", "CustomerId").count().show()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Grouping with Expressions
# MAGIC As we saw earlier, counting is a bit of a special case because it exists as a method. For this, usually we prefer to use the count function. Rather than passing that function as an expression into a select statement, we specify it as within agg. This makes it possible for you to pass-in arbitrary expressions that just need to have some aggregation specified. You can even do things like alias a column after transforming it for later use in your data flow.

# COMMAND ----------

display(
  df
  .groupBy(
    "InvoiceNo"
  )
  .agg(
    F.count("Quantity").alias("quan"),
    F.expr("count(Quantity)")
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Exercises

# COMMAND ----------

# MAGIC %md
# MAGIC a. Load the data and import `pyspark.sql.functions` with the alias `F`. Why does it make sense to `cache` it?

# COMMAND ----------

from pyspark.sql import functions as F

df = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/databricks-datasets/definitive-guide/data/retail-data/all/*.csv")
  .coalesce(5)
)
df.cache()
df.createOrReplaceTempView("dfTable")

# COMMAND ----------

# MAGIC %md
# MAGIC b. `display` the dataframe.

# COMMAND ----------

# Exercise 3.
display(df)

# COMMAND ----------

# MAGIC %md
# MAGIC c. How much has each customer spent in total?

# COMMAND ----------

display(
  df
  .groupBy(
    'CustomerID'
  )
  .agg(
    F.sum(F.col('Quantity') * F.col('UnitPrice'))
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC d. How much has does each customer on average spent on an invoice?

# COMMAND ----------

display(
  df
  .groupBy(
    'CustomerID',
    'InvoiceNo',
  )
  .agg(
    F.sum(F.col('Quantity') * F.col('UnitPrice')).alias('spent_on_invoice')
  )
  .groupBy(
    'CustomerID'
  )
  .agg(
    F.mean('spent_on_invoice')
  )
)

# COMMAND ----------

# MAGIC %md 
# MAGIC e. What is the the largest average amount a customer spents on an invoice?

# COMMAND ----------

display(
  df
  .groupBy(
    'CustomerID',
    'InvoiceNo',
  )
  .agg(
    F.sum(F.col('Quantity') * F.col('UnitPrice')).alias('spent_on_invoice')
  )
  .groupBy(
    'CustomerID'
  )
  .agg(
    F.mean('spent_on_invoice').alias('average_spent')
  )
  .agg(
    F.max('average_spent')
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC e. What is the longest description of a product? Use `pyspark.sql.functions.length`. Find the description of `pyspark.sql.functions.length` in the [docs](https://spark.apache.org/docs/2.3.0/api/python/index.html), it is in the `pyspark.sql.functions` module.

# COMMAND ----------

display(
  df
  .withColumn(
    'description_length',
    F.length('Description')
  )
  .agg(
    F.max('description_length')
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC f. Which country has bought the highest quantity of products? Construct a sorted list of quantity bought by each country.

# COMMAND ----------

display(
  df
  .groupBy(
    'Country'
  )
  .agg(
    F.sum('Quantity').alias('country_quantity')
  )
  .sort(
    F.col('country_quantity').desc()
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC g. Collect a list and a set of descriptions of products for each customer, how long are each list? Use `pyspark.sql.functions.size`.

# COMMAND ----------

display(
  df
  .groupBy(
    'CustomerID'
  )
  .agg(
    F.collect_set('Description').alias('set_of_descriptions'),
    F.collect_list('Description').alias('list_of_descriptions'),
  )
  .withColumn('length_of_set', F.size('set_of_descriptions'))
  .withColumn('length_of_list', F.size('list_of_descriptions'))
)

# COMMAND ----------

# MAGIC %md
# MAGIC h. How large is the average invoice for a customer? That is, if one concats the all unique description of products in a given envoice, how large is this list then on average? Use `pyspark.sql.functions.concat_ws` with `', '` as seperator.

# COMMAND ----------

display(
  df
  .groupBy(
    'CustomerID',
    'InvoiceNo'
  )
  .agg(
    F.concat_ws(', ', F.collect_set('Description')).alias('invoice')
  )
  .groupBy(
    'CustomerID'
  )
  .agg(
    F.mean(F.length('invoice')).alias('average_length')
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Window Functions
# MAGIC You can also use window functions to carry out some unique aggregations by either computing some aggregation on a specific “window” of data, which you define by using a reference to the current data. This window specification determines which rows will be passed in to this function. Now this is a bit abstract and probably similar to a standard group-by, so let’s differentiate them a bit more.
# MAGIC 
# MAGIC A group-by takes data, and every row can go only into one grouping. A window function calculates a return value for every input row of a table based on a group of rows, called a frame. Each row can fall into one or more frames. A common use case is to take a look at a rolling average of some value for which each row represents one day. If you were to do this, each row would end up in seven different frames. We cover defining frames a little later, but for your reference, Spark supports three kinds of window functions: ranking functions, analytic functions, and aggregate functions.
# MAGIC 
# MAGIC ![window_function](files/tables/window_functions.png)

# COMMAND ----------

# MAGIC %md
# MAGIC The first step to a window function is to create a window specification. Note that the partition by is unrelated to the partitioning scheme concept that we have covered thus far. It’s just a similar concept that describes how we will be breaking up our group. The ordering determines the ordering within a given partition, and, finally, the frame specification (the rowsBetween statement) states which rows will be included in the frame based on its reference to the current input row. In the following example, we look at all previous rows up to the current row:

# COMMAND ----------

from pyspark.sql import Window

df = df.withColumn("date", F.to_timestamp("InvoiceDate", "MM/d/yyyy H:mm"))
w = (
  Window
  .partitionBy(
    "CustomerId"
  )
  .orderBy(
    F.col("date")
  )
  .rowsBetween(
    Window.unboundedPreceding, Window.currentRow
  )
)



# COMMAND ----------

# MAGIC %md
# MAGIC A `Window` object has 4 method calls
# MAGIC 
# MAGIC   - `partitionBy(*cols)`: Describes how we will be breaking up our group.
# MAGIC   - `orderBy(*cols)`: Determines the ordering within a given partition.
# MAGIC   - `rowsBetween(start, end)`:  States which rows will be included in the frame based on its reference to the current input row. For example, “0” means “current row”, while “-1” means the row before the current row, and “5” means the fifth row after the current row. Stated in another way: the row_number based on the ordering has to be between [current + start, current + end] for the row to be included in the calculation.
# MAGIC   - `rangeBetween(start, end)`: States which rows will be included in the frame based on its reference to the value of the current input row. Because of this definition, when a RANGE frame is used, only a single ordering expression is allowed. For example, “0” means “current row”, while “-1” means one off before the current row, and “5” means the five off after the current row. Stated in another way: the value of the row in the ordering has to be between [current + start, current + end] for the row to be included in the calculation.
# MAGIC   
# MAGIC It is recommended to use `Window.unboundedPreceding`, `Window.unboundedFollowing`, and `Window.currentRow` to specify special boundary values.

# COMMAND ----------

# MAGIC %md
# MAGIC Now we want to use an aggregation function to learn more about each specific customer. An example might be establishing the maximum purchase quantity over time. 

# COMMAND ----------

display(
  df
  .dropDuplicates(['CustomerId', 'date'])
  .select(
    "CustomerId",
    "date",
    'Quantity',
  )
  .withColumn(
    'max_over_time',
    F.max('Quantity').over(w)
  )
  .sort(
    F.col('CustomerId').desc(),
    F.col('date').desc(),
  )
)

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC Spark SQL supports three kinds of window functions: ranking functions, analytic functions, and aggregate functions. The available ranking functions and analytic functions are summarized in the table below. For aggregate functions, users can use any existing aggregate function as a window function.
# MAGIC 
# MAGIC TODO: INSERT IMAGE from https://databricks.com/blog/2015/07/15/introducing-window-functions-in-spark-sql.html

# COMMAND ----------

# MAGIC %md
# MAGIC # Exercises

# COMMAND ----------

# MAGIC %md
# MAGIC a. Look up the methods of the `pyspark.sql.Window` class in the [docs](https://spark.apache.org/docs/2.3.0/api/python/pyspark.sql.html#pyspark.sql.Window). Have an extra look at `rangeBetween` and `rowsBetween`.

# COMMAND ----------

# MAGIC %md
# MAGIC b. What is the average quantity over time for each customer?

# COMMAND ----------

# MAGIC %md
# MAGIC c. What is the average quantity over the past week for each customer?

# COMMAND ----------

# MAGIC %md
# MAGIC d. Rank the quantity bought each day for each customer. Use `pyspark.sql.functions.dense_rank`, `pyspark.sql.functions.row_number` and `pyspark.sql.functions.rank`, what is the difference?

# COMMAND ----------

# MAGIC %md
# MAGIC e. What is the last time invoice date and the next invoice date at a given date for each customer? Use `pyspark.sql.functions.lag` and `pyspark.sql.functions.lead`.

# COMMAND ----------

# MAGIC %md
# MAGIC f. How many days are there between invoice dates? Use `pyspark.sql.functions.datediff`.

# COMMAND ----------

# MAGIC %md
# MAGIC g. What is the maximum unit price per country over time?

# COMMAND ----------

# MAGIC %md
# MAGIC h. What is the accumulated length of the description over time? `pyspark.sql.collect_set` and `pyspark.sql.collect_list` can be used over a window.

# COMMAND ----------

# MAGIC %md
# MAGIC i. Can you use do two `rowsBetween` method call on the samw `Window` object? Can you use do two for `rangeBetween`? 