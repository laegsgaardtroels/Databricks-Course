# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC DataFrames support two types of operations: transformations and actions.
# MAGIC 
# MAGIC Transformations, like select() or filter() create a new DataFrame from an existing one, resulting into another immutable DataFrame. All transformations are lazy. That is, they are not executed until an action is invoked or performed

# COMMAND ----------

from pyspark.sql import functions as F
from pyspark.sql import types as T

df = spark.range(0, 1000).withColumn('rand', F.rand()).select('rand')
df = df.distinct()
df.explain()

# COMMAND ----------

df = spark.range(0, 1000).withColumn('rand', F.rand()).select('rand')
df = df.distinct()
df.explain()
df.count()

# COMMAND ----------

df = spark.range(0, 100000000)
df.cache()
%timeit df.count()

# COMMAND ----------

df = spark.range(0, 100000000)
%timeit df.count()