# Databricks notebook source
# MAGIC %sh
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/airlines.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/airports.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/flights.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/planes.csv'
# MAGIC wget -P /tmp 'https://raw.githubusercontent.com/laegsgaardTroels/nycflights13/master/data-csv/weather.csv'

# COMMAND ----------

localpaths= [
  "file:/tmp/airlines.csv",
  "file:/tmp/airports.csv",
  "file:/tmp/flights.csv",
  "file:/tmp/planes.csv",
  "file:/tmp/weather.csv",
]
dbutils.fs.mkdirs("dbfs:/datasets/")
list(map(lambda localpath: dbutils.fs.cp(localpath, "dbfs:/datasets/"), localpaths))
display(dbutils.fs.ls("dbfs:/datasets/"))

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC Definitionally, a DataFrame consists of a series of records (like rows in a table), that are of type Row, and a number of columns (like columns in a spreadsheet) that represent a computation expression that can be performed on each individual record in the Dataset. Schemas define the name as well as the type of data in each column. Partitioning of the DataFrame defines the layout of the DataFrame or Dataset’s physical distribution across the cluster. The partitioning scheme defines how that is allocated. You can set this to be based on values in a certain column or nondeterministically.

# COMMAND ----------

airlines = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/datasets/airlines.csv")
)


# COMMAND ----------

# MAGIC %md
# MAGIC We discussed that a DataFame will have columns, and we use a schema to define them.

# COMMAND ----------

airlines.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC #Schemas
# MAGIC 
# MAGIC A schema defines the column names and types of a DataFrame. We can either let a data source define the schema (called schema-on-read) or we can define it explicitly ourselves. For ad hoc analysis, schema-on-read usually works just fine. When using Spark for production Extract, Transform, and Load (ETL), it is often a good idea to define your schemas manually, especially when working with untyped data sources like CSV and JSON because schema inference can vary depending on the type of data that you read in.
# MAGIC 
# MAGIC The schema is an attribute af a pyspark DataFrame as you can see below.

# COMMAND ----------

airlines.schema

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC A schema is a StructType made up of a number of fields, StructFields, that have a name, type, a Boolean flag which specifies whether that column can contain missing or null values, and, finally, users can optionally specify associated metadata with that column. The metadata is a way of storing information about this column (Spark uses this in its machine learning library).
# MAGIC 
# MAGIC Here is how to create and enforce a specific schema on a DataFrame.

# COMMAND ----------

from pyspark.sql import types as T

manual_schema = T.StructType([
  T.StructField("carrier", T.StringType(), True),
  T.StructField("name", T.StringType(), True, metadata={"hello":"world"}),
])
df = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .schema(manual_schema)
  .load("dbfs:/datasets/airlines.csv")
)
display(df)

# COMMAND ----------

# MAGIC %md
# MAGIC If the types in the data (at runtime) do not match the schema, Spark can throw an error, depending on how the `mode` option is set.

# COMMAND ----------

from pyspark.sql import types as T

manual_schema = T.StructType([
  T.StructField("carrier", T.StringType(), False),
  T.StructField("name", T.ByteType(), True, metadata={"hello":"world"}),
])
df = (
  spark
  .read
  .format("csv")
  .schema(manual_schema)
  .option("mode", "FAILFAST")
  .load("dbfs:/datasets/airlines.csv")
)
df.collect()

# COMMAND ----------

# MAGIC %md
# MAGIC # Columns and Expressions
# MAGIC 
# MAGIC Columns in Spark are similar to columns in a spreadsheet, R dataframe, or pandas DataFrame. You can select, manipulate, and remove columns from DataFrames and these operations are represented as expressions.
# MAGIC 
# MAGIC To Spark, columns are logical constructions that simply represent a value computed on a per-record basis by means of an *expression*.

# COMMAND ----------

# MAGIC %md
# MAGIC #Columns
# MAGIC There are a lot of different ways to construct and refer to columns but the two simplest ways are by using the col or column functions. To use either of these functions, you pass in a column name:

# COMMAND ----------

from pyspark.sql import functions as F

F.col("someColumnName")
F.column("someColumnName")

# COMMAND ----------

# MAGIC %md
# MAGIC You can also refer directly to the columns of a DataFrame, this can be useful when joining two DataFrame's with the same column name.

# COMMAND ----------

airlines['carrier']

# COMMAND ----------

# MAGIC %md
# MAGIC Columns are expressions, but what is an expression? An expression is a set of transformations on one or more values in a record in a DataFrame.

# COMMAND ----------

(((F.col("someCol") + 5) * 200) - 6) < F.col("otherCol")

# COMMAND ----------

# MAGIC %md
# MAGIC If you want to programmatically access columns, you can use the columns property to see all columns on a DataFrame.

# COMMAND ----------

airlines.columns

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # Basic DataFrame Manipulation
# MAGIC Now that we briefly defined the core parts of a DataFrame, we will move onto manipulating DataFrames. When working with individual DataFrames there are some fundamental objectives almost all of these can be accomplished with below DataFrame methods and the functions in the `pyspark.sql.functions` module.
# MAGIC 
# MAGIC - `where()` and `filter()` Filter rows based on a boolean expression.
# MAGIC - `sort()` and `orderBy()` Orders by a given column or more columns.
# MAGIC - `select()` and `selectExpr()` Select columns of a data frame.
# MAGIC - `withColumn()` Adds a new column, which is a function of existing columns.
# MAGIC - `agg()` Can condense multiple records to a single record.
# MAGIC 
# MAGIC The `agg()` method can be used in conjuction with `groupBy()` which changes the scope of each function from operating on the entire DataFrame to operating on it group-by-group.

# COMMAND ----------

# MAGIC %md
# MAGIC # `where()` and `filter()`
# MAGIC To filter rows, we create an expression that evaluates to true or false. You then filter out the rows with an expression that is equal to false. The most common way to do this with DataFrames is to create either an expression as a String or build an expression by using a set of column manipulations. 
# MAGIC 
# MAGIC Use the comparison operators: `>`, `>=`, `<,` `<=`, `!=` (not equal), `==` (equal) and `.eqNullSafe()`.
# MAGIC 
# MAGIC Every boolean expression must be `True` in order for a row to be included in the output. For other types of combinations you'll need to use the Boolean operators: `&` (and), `|` (or), `~` (not), `^` (XOR).

# COMMAND ----------

flights = (
  spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("dbfs:/datasets/flights.csv")
)

display(
  flights
  .where(
    F.col('month') > 6
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC Instinctually, you might want to put multiple filters into the same expression. Although this is possible, it is not always useful, because Spark automatically performs all filtering operations at the same time regardless of the filter ordering. This means that if you want to specify multiple AND filters, just chain them sequentially and let Spark handle the rest.

# COMMAND ----------

display(
  flights
  .where(
    F.col('month') > 6
  )
  .where(
    F.col('month') < 8
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Missing Values
# MAGIC 
# MAGIC When filtering `Null` values are evaluated to `False`, this can give surprising results.

# COMMAND ----------

display(
  flights
  .where(
    F.lit(None) == F.lit(None)
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC The above returns an empty DataFrame because it evaluates to `Null`.

# COMMAND ----------

display(
  flights
  .select(
    (F.lit(None) == F.lit(None)).alias('null_equal_null'),
    (F.lit(None) == F.lit(10)).alias('null_equal_number'),
    (F.lit(None) > F.lit(10)).alias('null_gt_number'),
    (F.lit(None) + F.lit(10)).alias('null_add_number'),
    (F.lit(None) * F.lit(10)).alias('null_multiply_number'),
    (F.lit(None) / F.lit(10)).alias('null_division_number'),
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC The `Column` functions `isNotNull()`, `isNull()` can be used to filter `Null` values away, replace them or whatever is the best action in the given analysis. A handy function in the `pyspark.sql.functions` module is `coalesce()`, it takes the first value in a series of column expressions which is not `Null`, analogous to the SQL function. 

# COMMAND ----------

display(
  flights
  .withColumn(
    'null_literal',
    F.lit(None)
  )
  .select(
    F.lit(None).isNotNull().alias('isNotNull'),
    F.lit(None).isNull().alias('isNull'),
    F.coalesce(F.lit(None), F.lit(True)).alias('coalesce'),
    F.lit(None).eqNullSafe(F.lit(None)).alias('eqNullSafe'),
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Exercises
# MAGIC 
# MAGIC Find all flights that:

# COMMAND ----------

# MAGIC %md a. Had an arrival delay of two or more hours (Hint: arr_delay contains arrival delay in minutes).

# COMMAND ----------

# MAGIC %md b. Flew to Houston (IAH or HOU) (Hint: dest contains flight destination).

# COMMAND ----------

# MAGIC %md c. Departed in summer (July, August and September) (Hint: month contains month of departure).

# COMMAND ----------

# MAGIC %md d. Arrived more than two hours late, but didn’t leave late (Hint: dep_delay and arr_delay contans departure and arrival delays, in minutes. Negative times represent early departures/arrivals).

# COMMAND ----------

# MAGIC %md e. Were delayed by at least an hour, but made up over 30 minutes in flight (Hint: air_time contains the amount of time spent in the air, in minutes).

# COMMAND ----------

# MAGIC %md f. How many flights have a missing dep_time (Hint: Use is.na and count).

# COMMAND ----------

# MAGIC %md g. Another useful filtering helper is the Column method `between()`. What does it do? Look into the [docs](https://spark.apache.org/docs/2.3.2/api/python/pyspark.sql.html#pyspark.sql.Column.between).

# COMMAND ----------

# MAGIC %md 
# MAGIC h. A lazy way to deal with `Null` values is to use the `DataFrame` method `fillna()` it can be used to replace ALL `Null` values in a DataFrame. Use it to replace `Null` values with 1.

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC # `sort()` and `orderBy()`
# MAGIC 
# MAGIC Arrange rows with `sort()` and `orderBy()`, they work equivalently. They accept both column expressions and strings as well as multiple columns. The default is to sort in ascending order.

# COMMAND ----------

display(
  flights
  .where(
    F.col("arr_delay").isNotNull()
  )
  .sort(
    "arr_delay"
  )
)

# COMMAND ----------

# MAGIC %md To sort descending you need to specify this explicitely.

# COMMAND ----------

display(
  flights
  .where(
    F.col("arr_delay").isNotNull()
  )
  .sort(
    F.col("arr_delay").desc()
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # `select()` and `selectExpr()`
# MAGIC 
# MAGIC `select()` and `selectExpr()` allow you to do the DataFrame equivalent of SQL queries on a table of data.

# COMMAND ----------

display(
  flights
  .select(
    'year',
    'month',
    'day'
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC You can refer to columns in a number of different ways.

# COMMAND ----------

display(
  flights
  .select(
    F.col('year'),
    F.column('year'),
    F.expr('year'),
    flights['year'],
    'year'
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC As we’ve seen thus far, expr is the most flexible reference that we can use. It can refer to a plain column or a string manipulation of a column. Because select followed by a series of expr is such a common pattern, Spark has a shorthand for doing this efficiently: `selectExpr()`. 

# COMMAND ----------

display(
  flights
  .selectExpr(
    'year',
    'month',
    'day',
    'year + 1 AS year_plus_1'
  )
)

# COMMAND ----------

# MAGIC %md 
# MAGIC ## Dropping columns
# MAGIC You likely already noticed that we can drop columns using `select()` e.g. select all but the variables you want to drop. However, there is also a dedicated method called `drop()`.

# COMMAND ----------

display(
  flights
  .select(
    'year',
    'month',
    'day'
  )
  .drop(
    'year'
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Exercises

# COMMAND ----------

# MAGIC %md
# MAGIC a. How could you use `orderBy()` to sort all missing values of dep_time to the start?

# COMMAND ----------

# MAGIC %md
# MAGIC b. Sort flights to find the most delayed flights.

# COMMAND ----------

# MAGIC %md
# MAGIC c. Sort flights to find the fastest flights.

# COMMAND ----------

# MAGIC %md
# MAGIC d. Which flights travelled the longest? Which travelled the shortest?

# COMMAND ----------

# MAGIC %md
# MAGIC e. Select 3 variables, using 3 different methods.

# COMMAND ----------

# MAGIC %md
# MAGIC f. What happens if you include the name of a variable multiple times in a `select()` call?

# COMMAND ----------

# MAGIC %md g. What does `'*'` do in a `select()`? Try.

# COMMAND ----------

# MAGIC %md
# MAGIC h. Select all columns that starts with: `'dep'`.
# MAGIC 
# MAGIC Hint: `.select(*[c for c in df.columns if c.startswith('arr')])` can be used to select all columns that starts with `'arr'`, this utilizes Pythons list compression, the asterik (`*`) in python unpacks list as arguments to the method call, and the `if c.startswith('arr')` filters out all strings in df.columns list which that does not start with `'arr'`.

# COMMAND ----------

# MAGIC %md i. Drop all columns which starts with `'dep'` using list compression.

# COMMAND ----------

# MAGIC %md
# MAGIC # `withColumn()`
# MAGIC 
# MAGIC The `withColumn()` adds a new column, which is a function of existing columns. It takes two arguments: the column name and the expression that will create the value for that given row in the DataFrame.

# COMMAND ----------

# MAGIC %md
# MAGIC ## Renaming columns
# MAGIC You can rename a column with the `withColumn()` function, e.g. `.withColumn(foo, foo)`, but it is better to use the dedicated function `ẁithColumnRenamed()` for this.

# COMMAND ----------

# MAGIC %md
# MAGIC # `agg()`
# MAGIC 
# MAGIC `agg()` can condense multiple records to a single record. The `agg()` method can be used in conjuction with `groupBy()` which changes the scope of each function from operating on the entire DataFrame to operating on it group-by-group. This is useful to compute aggregate statistics of the records in the DataFrame, to answer questions like: What is the average month a flight took place, average year, max year, a list of all years, a list of all months.

# COMMAND ----------

display(
  flights
  .agg(
    F.mean('month'),
    F.mean('year'),
    F.max('year'),
    F.collect_set('year'),
    F.collect_set('month'),
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC # Exercises

# COMMAND ----------

# MAGIC %md a. The `count()` function in `pyspark.sql.functions` counts all non-null records in a given column. Use it to count all non null departure delays. 

# COMMAND ----------

display(
  flights
  .agg(
    F.count('dep_delay')
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC b. The `F.count('*')` is a shorthand for calculating all columns, use it to calculate how big a percentage of flights has a null value in their departure delay.

# COMMAND ----------

# MAGIC %md
# MAGIC c. The `countDistinct` function in `pyspark.sql.functions` can be used to calculate the distinct values in one or more columns. Use it to calculate the number of distinct destinations.

# COMMAND ----------

display(
  flights
  .agg(
    F.countDistinct('dest')
  )
)

# COMMAND ----------

# MAGIC %md
# MAGIC d. Currently `dep_time` is in the format HHMM or HMM, extract the actual departure hour and minute into two new columns: `dep_hour` and `dep_minute` (Hint: Use integer division `dep_time // 100` and modulo `dep_time % 100`).

# COMMAND ----------

# MAGIC %md
# MAGIC e. Collect all distinct destinations into a set, use `collect_set()` from `pyspark.sql.functions.`

# COMMAND ----------

# MAGIC %md
# MAGIC f. Create a map from origin to destination using `create_map()` from `pyspark.sql.functions`

# COMMAND ----------

display(
  flights
  .withColumn(
    'origin_dest',
    F.create_map(F.col('origin'), F.col('dest'))
  )
)